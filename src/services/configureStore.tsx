import { createStore, applyMiddleware, compose } from 'redux'
import session from 'reducers'
import thunk from 'redux-thunk'

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
}


function saveToLocalStorage(state: any) {
    try {
        const serializedState = JSON.stringify(state)
        sessionStorage.setItem('state', serializedState)
    }
    catch (e) {
        return undefined
    }
}
function loadFromLocalStorage() {
    try {
        const serializedState = sessionStorage.getItem('state')
        if (serializedState === null) return undefined
        return JSON.parse(serializedState)
    }
    catch (e) {
        return undefined
    }
}
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

function configureStore() {
    const persistedState = loadFromLocalStorage()
    let store = createStore(session,persistedState, composeEnhancers(applyMiddleware(thunk)))

    store.subscribe(() => saveToLocalStorage(store.getState()))
    return store
}

export default configureStore()


// export default createStore(session, applyMiddleware(thunk))
