import axios from 'axios';
export default class ServiceState{
    static setAuthToken = (token:string="") => {
        axios.defaults.headers.post['Content-Type'] = 'application/json';
        if (token) {
          // Store for future scopes
          localStorage.setItem('token',token)
          // Apply to every request
          axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
        }
        else {
          // Delete auth header
          delete axios.defaults.headers.common["Authorization"];
        }
      };
      static setBaseURL=()=>{
        axios.defaults.baseURL = process.env.REACT_APP_BASE_URL;
      }
}