import React from 'react';
import styles from './ComingSoon.module.scss';

const ComingSoon: React.FC = () => (
  <h1 className={styles.ComingSoon}>
    THis Page is Comming Soon
  </h1>
);

export default ComingSoon;
