import { MDBContainer } from 'mdbreact';
import React from 'react';
import styles from './FAQs.module.scss';
import bgImage from 'assets/images/contact-us.jpg'
import { Collapse } from 'antd';
import CollapsePanel from 'antd/lib/collapse/CollapsePanel';

const faqs = [
  {
    question: "Mein Esa Kyu hu?",
    answer: "Karna hai kya mujhko ye maine kab hai jaana. Lagta hai gaaunga zindagi bhar bas ye gaana. Hoga jaane mera ab kya.Oh…Koyi toh bataaye mujhe Oh…Gadbad hai ye sab kya"
  },
  {
    question: "Mein Esa Kyu hu?",
    answer: "Karna hai kya mujhko ye maine kab hai jaana. Lagta hai gaaunga zindagi bhar bas ye gaana. Hoga jaane mera ab kya.Oh…Koyi toh bataaye mujhe Oh…Gadbad hai ye sab kya"
  },
  {
    question: "Mein Esa Kyu hu?",
    answer: "Karna hai kya mujhko ye maine kab hai jaana. Lagta hai gaaunga zindagi bhar bas ye gaana. Hoga jaane mera ab kya.Oh…Koyi toh bataaye mujhe Oh…Gadbad hai ye sab kya"
  },
  {
    question: "Mein Esa Kyu hu?",
    answer: "Karna hai kya mujhko ye maine kab hai jaana. Lagta hai gaaunga zindagi bhar bas ye gaana. Hoga jaane mera ab kya.Oh…Koyi toh bataaye mujhe Oh…Gadbad hai ye sab kya"
  }
]
const FAQs: React.FC = () => (

  <div className={styles.FAQs}>
    <div className={styles.FAQImageContainer} style={{ backgroundImage: `url(${bgImage})` }}>
      <MDBContainer className='text-right pt-5'>
        <div className={styles.TopHead}>Frequently Asked Questions</div>
        <div className={styles.TopSubHead}>We are her for you...</div>
      </MDBContainer>

    </div>
    <MDBContainer className=' py-5'>
      <Collapse defaultActiveKey={['1']} bordered={false} ghost>

        {
          faqs.map((e, i) => (
            <CollapsePanel className={styles.collapsePanel}  header={e.question} key={i + 1}>
              <p className={styles.answer}>{e.answer}</p>
            </CollapsePanel>

            // <div>
            //   <p><span className="bold text-red">Q. </span> {e.question}</p>
            //   <p className="bold"><span className="bold text-red">A. </span> {e.answer}</p>
            // </div>
          ))
        }
      </Collapse>
    </MDBContainer>
  </div>
);

export default FAQs;
