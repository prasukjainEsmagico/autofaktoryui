import React from 'react';
import styles from './HowItWorks.module.scss';
import sampleImage from "assets/images/LogoOfpress/car12.jpg"
import { MDBCol, MDBContainer, MDBRow } from 'mdbreact';

const content = [
  {
    title: "The quick brown fox jumps over the lazy dog",
    image: sampleImage,
    description: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."
  },
  {
    title: "The quick brown fox jumps over the lazy dog",
    image: sampleImage,
    description: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."
  },
  {
    title: "The quick brown fox jumps over the lazy dog",
    image: sampleImage,
    description: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."
  }
]

const HowItWorks: React.FC = () => (
  <div className={styles.HowItWorks}>

    <h1 className={styles.sectionTitle}>
        How AutoFaktory.com Works
    </h1>
    <MDBContainer>
      {
        content.map((e, i) => (
          < MDBRow className={`${styles.contentRow} ${i % 2 == 1 && 'flex-row-reverse '}`}>
            
            <MDBCol md="6" className={`${i % 2 == 1 ? 'text-right pr-4':'pl-4'}`} >
              <img src={e.image} className={styles.rowImage} alt="" />
            </MDBCol>
            <MDBCol md="6" className={styles.contentCol} >
              <div className={styles.rowTitle}>{e.title}</div>
              <div className={styles.rowDescription}>{e.description}</div>
            </MDBCol>
          </MDBRow>
        ))
      }
    </MDBContainer>
  </div >
);

export default HowItWorks;
