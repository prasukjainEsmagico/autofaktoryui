import { Button, Col, Divider, Image, Row, Tag } from 'antd';
import { iCarDetails } from 'common/interfaces/interfaces';
import Loader from 'components/common/Loader/Loader';
import { MDBContainer } from 'mdbreact';
import { number } from 'prop-types';
import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import styles from './BuyCarDetails.module.scss';
import { getCar } from 'actions'
import { connect } from 'react-redux';
import Icon, { FileImageFilled, RedoOutlined, StarFilled, BoxPlotFilled, CalendarFilled, CarFilled } from '@ant-design/icons';
import TestDriveModal from 'components/buyCarDetails/TestDriveModal/TestDriveModal';

interface iRouteProps {
  id: string
}
interface iMapDispatchToProps {
  getCar: any,
  // getBuyCarsData:any
}

interface iMapStateToProps {
}
interface iProps extends RouteComponentProps<iRouteProps>, iMapStateToProps, iMapDispatchToProps {

}
interface iState {
  car: iCarDetails | null
  subcomponent:any
}
class BuyCarDetails extends React.Component<iProps, iState>{

  constructor(props: iProps) {
    super(props);
    this.state = {
      car: null,subcomponent:""
    }
  }
  componentDidMount() {
    this.props.getCar(this.props.match.params.id).then((e: iCarDetails) => {
      this.setState({ car: e })
    })
  }
  render() {
    const { car,subcomponent } = this.state
    return (
      <div className={styles.BuyCarDetails}>
        {this.state.car ?

          <MDBContainer>
            <Row gutter={24}>
              <Col xs={24} sm={24} md={12} lg={16} xl={16}>
                <Image src={this.state.car.image_url} />
                <Row gutter={24} className="my-2  ">
                  <Col span={12}><Button className={styles.ImageButtons} color="dark" block><FileImageFilled />Image Gallery({0})</Button></Col>
                  <Col span={12}><Button className={styles.ImageButtons} color="dark" block><RedoOutlined />360&deg; View</Button></Col>
                </Row>
              </Col>
              <Col xs={24} sm={24} md={12} lg={8} xl={8} className="p-2">
                <div className={styles.carModal}>
                  {car?.make} {car?.model__model}
                </div>
                <div className={styles.carVariant}>
                  {car?.varient__name}
                </div>
                <div className={styles.carPrice}>
                  &#8377;{car?.price}
                </div>

                <div className={styles.testDriveContainer}>
                  Interested in the car. Go for a test drive.<br />
                  <Button className={styles.TestDriveButton} onClick={this.showTestDrive.bind(this)}>Book a test drive</Button>
                </div>
              </Col>

            </Row>

            <Row className={styles.CarBasicRow} style={{ margin: "15px 0 " }} gutter={64} justify="space-between">
              <Tag className={styles.CarBasicFeature} >
                <StarFilled /><br />
                {car?.condition}
              </Tag>

              <Tag className={styles.CarBasicFeature} style={{ lineHeight: "8px" }}>
                <div className={styles.carColor} style={{ backgroundColor: car?.color }} /><br />
              Tint
              </Tag>

              <Tag className={styles.CarBasicFeature} >
                <CarFilled /><br />
                {car?.km}kms
              </Tag>
              <Tag className={styles.CarBasicFeature} >
                <CalendarFilled /><br />
                {car?.registrationYear}
              </Tag>
              <Tag className={styles.CarBasicFeature} >
                <BoxPlotFilled /><br />
                {car?.transmission}
              </Tag>
              <Tag className={styles.CarBasicFeature} >
                <i className="fas fa-gas-pump"></i><br />
                {car?.varient__fuelType}
              </Tag>



            </Row>

            <Divider orientation="left"  ><span className={styles.DeviderText}> All Features</span></Divider>
            <Row justify="space-between">
              {car?.features.map(e=>(
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  {e.feature__name}:
                </span>
                {e.value}
              </Col>
              ))}
            </Row>

            <Divider orientation="left"  ><span className={styles.DeviderText}> All Specifications</span></Divider>
            <Row justify="space-between">
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  DriveTrain:
                </span>
                {car?.other_specs.drivetrain}
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Engine Type:
                </span>
                {car?.other_specs.engineType}
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Valve Cylender Configuration:
                </span>
                {car?.other_specs.valveCylenderConfig}
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Maximum Power:
                </span>
                {car?.other_specs.maxPower}
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Maximum Torque:
                </span>
                {car?.other_specs.maxTorque}
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Front Suspension:
                </span>
                {car?.other_specs.suspensionFront}
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Rear Suspension:
                </span>
                {car?.other_specs.suspensionRear}
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Steering Type:
                </span>
                {car?.other_specs.steeringType}
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Wheels:
                </span>
                {car?.other_specs.wheels}
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Spare Wheels:
                </span>
                {car?.other_specs.spareWheels}
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Front Break Type:
                </span>
                {car?.other_specs.frontBreakType}
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  RearBreakType:
                </span>
                {car?.other_specs.rearBreakType}
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Front Tyre:
                </span>
                {car?.other_specs.frontTyre}
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Rear Tyre:
                </span>
                {car?.other_specs.rearTyre}
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Length:
                </span>
                {car?.other_specs.dimLength}cm
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Width:
                </span>
                {car?.other_specs.dimWidth}cm
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Height:
                </span>
                {car?.other_specs.dimHeight}cm
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Seeting Capacity:
                </span>
                {car?.other_specs.seetingCapacity}
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Displacement:
                </span>
                {car?.other_specs.displacement}cm
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Wheel Base:
                </span>
                {car?.other_specs.wheelbase}cm
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Ground Clearance:
                </span>
                {car?.other_specs.groundClearance}cm
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Kerb Weight:
                </span>
                {car?.other_specs.kerbWeight}kg
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Door:
                </span>
                {car?.other_specs.door}
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Seeting Rows:
                </span>
                {car?.other_specs.seetingRows}
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Boot Space:
                </span>
                {car?.other_specs.bootSpace}cm
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Fuel Capacity:
                </span>
                {car?.other_specs.fuelCapacity}Litter
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Mileage:
                </span>
                {car?.other_specs.mileage}kmpl
              </Col>
              <Col span="12" className={styles.otherSpecsCol}>
                <span className="bold mr-3">
                  Turning Radius:
                </span>
                {car?.other_specs.turningRadius}cm
              </Col>
            </Row>
          </MDBContainer>
          :
          <Loader />
        }
        {subcomponent}
      </div>
    );
  }
  showTestDrive(){
    let subcomponent=<TestDriveModal car={this.props.match.params.id} onCloseModal={()=>this.setState({subcomponent:""})} />
    this.setState({subcomponent})
  }
}

const mapStateToProps = (state: any) => ({
  // ...state.filters
})
export default connect<iMapStateToProps, iMapDispatchToProps>(mapStateToProps, { getCar })(withRouter<any, any>(BuyCarDetails));
