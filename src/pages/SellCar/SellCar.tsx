import { MDBCol, MDBContainer, MDBRow } from 'mdbreact';
import React from 'react';
import styles from './SellCar.module.scss';
import bgImage from 'assets/images/contact-us.jpg'
import SellCarWizard from 'components/sell/SellCarWizard/SellCarWizard';
import sampleImage from "assets/images/LogoOfpress/car12.jpg"

const content = [
  {
    image: sampleImage,
    description: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr"
  },
  {
    image: sampleImage,
    description: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr"
  },
  {
    image: sampleImage,
    description: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr"
  }
]
const SellCar: React.FC = () => (
  <div className={styles.SellCar}>
    <div className={styles.Hero} style={{ backgroundImage: `url(${bgImage})` }}>
      <MDBContainer className='text-right pt-5'>
        <div className={styles.TopHead}>Sell Cars</div>
        <div className={styles.TopSubHead}>We will make sure it goes to a safe home... </div>
      </MDBContainer>
    </div>
      <SellCarWizard />
      <MDBContainer>
        <h2 className="text-center">Sell Your Car in simple steps</h2>
        < MDBRow className={styles.contentRow}>
          {
            content.map((e, i) => (
            <MDBCol md="4" className={styles.contentCol} >
              <div className={styles.StepNumber}>{i+1}</div>
              <div className={styles.rowDescription}>{e.description}</div>
            </MDBCol>
            ))
          }
        </MDBRow>
      </MDBContainer>
  </div>
);

export default SellCar;
