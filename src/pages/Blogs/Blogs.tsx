import { MDBCol, MDBContainer, MDBRow } from 'mdbreact';
import React from 'react';
import styles from './Blogs.module.scss';
import bgImage from 'assets/images/contact-us.jpg'

const blog = [
  {
    title: "Mein Kampf",
    abstract: "On April 1st, 1924, I began to serve my sentence of detention in the Fortress of Landsberg am Lech, following the verdict of the Munich People’s Court of that time.\nAfter years of uninterrupted labour it was now possible for the first time to begin a  work  which  many  had  asked  for  and  which  I  myself  felt  would  be  profitable  for the Movement. So I decided to devote two volumes to a description not only of  the  aims  of  our  Movement  but  also  of  its  development.  There  is  more  to  be  learned from this than from any purely doctrinaire treatise.  This has also given me the opportunity of describing my own development in so far as such a description is necessary to the understanding of the first as well as the  second  volume  and  to  destroy  the  legendary  fabrications  which  the  Jewish  Press have circulated about me.",
    author: "Adolf Hitler",
    url: "https://en.wikipedia.org/wiki/Adolf_Hitler",
    published_date: "December 20, 2018",
    tags: ["Hitler", "Germany", "Philosophy","Philosophy","Philosophy"],
  }
]
// 
const Blogs: React.FC = () => (
  <div className={styles.BlogsContainer}  >
    <div className={styles.Blogs} style={{ backgroundImage: `url(${bgImage})` }}>
      <MDBContainer className='text-right pt-5'>
        <div className={styles.TopHead}>Blogs</div>
        <div className={styles.TopSubHead}>Checkout We Wrote For You...</div>
      </MDBContainer>
    </div>
    <MDBContainer className=' pt-5'>
        {blog.map(e => (
          <MDBCol  className={"card p-3 mb-5   " + styles.blog} onClick={() => window.open(e.url, "_blank")} >
            <h3 className={styles.title}>{e.title}</h3>
            <MDBRow between>
              <div className='pl-3'>{e.published_date}</div>
              <div className="pr-3 bold">- By {e.author}</div>
            </MDBRow>
            <p className="my-2">
              {e.tags.map(tag => <span className={styles.tag}>{tag}</span>)}
            </p>
            <p className={styles.abstract}>{e.abstract}</p>
          </MDBCol>
        ))}
    </MDBContainer>
  </div >
);

export default Blogs;
