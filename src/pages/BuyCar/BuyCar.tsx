import { Row } from 'antd';
import CarsPagination from 'components/buy/CarsPagination/CarsPagination';
import FiltersContainer from 'components/buy/FiltersContainer/FiltersContainer';
import { MDBContainer } from 'mdbreact';
import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import styles from './BuyCar.module.scss';


interface iState {
}
interface iProps extends RouteComponentProps {

}

class BuyCar extends React.Component<iProps, iState>{

  render() {
    return (
      <div className={styles.BuyCar}>
        <Row>
          <FiltersContainer />
          <CarsPagination />
        </Row>
      </div>
    );
  }
}

export default withRouter(BuyCar);
