import HeroComponent from 'components/Home/HeroComponent/HeroComponent';
import { MDBCarousel, MDBCarouselInner, MDBCol, MDBContainer, MDBMask, MDBRow, MDBView } from 'mdbreact';
import React from 'react';
import styles from './Home.module.scss';
import howItWorksImage1 from 'assets/images/Imagesofteam/car11.jpg'
import howItWorksImage2 from 'assets/images/Imagesofteam/car13.jpg'
import howItWorksImage3 from 'assets/images/Imagesofteam/car12.jpg'
import qualityAssuredImage from 'assets/images/quality-assured AF.svg'
import CarTypeGrid from 'components/Home/CarTypeGrid/CarTypeGrid';

import Carousel from "react-multi-carousel";
import RecommendedCars from 'components/common/RecommendedCars/RecommendedCars';
import Testimonials from 'components/common/Testimonials/Testimonials';
import Footer from 'components/common/Footer/Footer';
import HomeCarFilters from 'components/Home/HomeCarFilters/HomeCarFilters';

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
    slidesToSlide: 3 // optional, default to 1.
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
    slidesToSlide: 2 // optional, default to 1.
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    slidesToSlide: 1 // optional, default to 1.
  }
};
const Home: React.FC = () => {
  const width = window.innerWidth

  return (
    <div className={styles.Home} >
      <HeroComponent />
      <section className={styles.howItWorksContainer}>
        <MDBContainer>
          <h1 className="text-center my-4 text-light">How It Works  </h1>
          <Carousel
            swipeable={true}
            draggable={true}
            showDots={true}
            responsive={responsive}
            infinite={true}
            autoPlay={width < 1024}
            keyBoardControl={true}
            transitionDuration={500}
            containerClass="carousel-container"
            removeArrowOnDeviceType={["tablet", "mobile"]}
            dotListClass="custom-dot-list-style"
            itemClass="carousel-item-padding-40-px"
          >
            <div className="card mb-5 mx-2" >
              <MDBView className="overlay z-depth-1" waves>
                <img
                  src={howItWorksImage1}
                  alt=""
                  className="img-fluid"
                />
                <a href="#!">
                  <MDBMask overlay="white-slight" />
                </a>
              </MDBView>
              <div className="text-center" style={{ padding: "1rem" }}>
                <h5 className={"font-weight-bold mb-3 " + styles.cardHead}>Great value cars</h5>
                <p className={styles.cardBody} >We own all the cars we list and have thoroughly inspected and reconditioned each one to the highest standards.</p>
              </div>
            </div>
            <div className="card mb-5 mx-2" >
              <MDBView className="overlay z-depth-1" waves>
                <img
                  src={howItWorksImage2}
                  alt=""
                  className="img-fluid"
                />
                <a href="#!">
                  <MDBMask overlay="white-slight" />
                </a>
              </MDBView>
              <div className="text-center" style={{ padding: "1rem" }}>
                <h5 className={"font-weight-bold mb-3 " + styles.cardHead}>Buy entirely online</h5>
                <p className={styles.cardBody} >Get your car delivered to your door at a time that suits you.</p>
              </div>
            </div>
            <div className="card mb-5 mx-2" >
              <MDBView className="overlay z-depth-1" waves>
                <img
                  src={howItWorksImage3}
                  alt=""
                  className="img-fluid"
                />
                <a href="#!">
                  <MDBMask overlay="white-slight" />
                </a>
              </MDBView>
              <div className="text-center" style={{ padding: "1rem" }}>
                <h5 className={"font-weight-bold mb-3 " + styles.cardHead}>Great value cars</h5>
                <p className={styles.cardBody} >Select from our wide range of high-quality cars and complete your purchase or financing fully online.</p>
              </div>
            </div>
          </Carousel>
        </MDBContainer>
      </section>
      <section className={styles.buyingConfidenceContainer}>
        <MDBContainer >
          <h2 className="h1-responsive text-center font-weight-bold mb-5" >
            Complete car buying confidence
                    </h2>
          <Carousel
            swipeable={true}
            draggable={true}
            showDots={true}
            responsive={responsive}
            infinite={true}
            autoPlay={width < 1024}
            keyBoardControl={true}
            transitionDuration={500}
            containerClass="carousel-container"
            removeArrowOnDeviceType={["tablet", "mobile"]}
            dotListClass="custom-dot-list-style"
            itemClass="carousel-item-padding-40-px"
          >
            {/* <div style={{ padding: "0 10x 0 10px" }}> */}
            {/* <MDBRow between> */}

            <div className=" px-2 text-center">
              <img src={qualityAssuredImage} alt="thumbnail" className={styles.qualityAssuredImage} />
              <h5 className={"font-weight-bold mb-3 " + styles.cardHead}>AutoFaktory Quality Assured</h5>
              <p className={styles.cardBody}>Not only are our cars thoroughly checked and reconditioned, but each one has had a recent service and MOT (if required) and also shows the number of previous owners.</p>
            </div>
            <div className=" px-2 text-center">
              <img src={qualityAssuredImage} alt="thumbnail" className={styles.qualityAssuredImage} />
              <h5 className={"font-weight-bold mb-3 " + styles.cardHead}>AutoFaktory Quality Assured</h5>
              <p className={styles.cardBody}>Not only are our cars thoroughly checked and reconditioned, but each one has had a recent service and MOT (if required) and also shows the number of previous owners.</p>


            </div>
            <div className=" px-2 text-center">
              <img src={qualityAssuredImage} alt="thumbnail" className={styles.qualityAssuredImage} />
              <h5 className={"font-weight-bold mb-3 " + styles.cardHead}>AutoFaktory Quality Assured</h5>
              <p className={styles.cardBody}>Not only are our cars thoroughly checked and reconditioned, but each one has had a recent service and MOT (if required) and also shows the number of previous owners.</p>
            </div>
          </Carousel>
        </MDBContainer>
      </section>
      <CarTypeGrid />
      <Testimonials />
      <HomeCarFilters/>
    </div>
  );
}
export default Home;
