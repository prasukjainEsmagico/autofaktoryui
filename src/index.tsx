import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { configureStore as store, ServiceState } from 'services/index'

import {
  Provider
} from 'react-redux';

import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from 'react-router-dom';
ServiceState.setBaseURL()
let token = localStorage.getItem('token')
if (token) {
  ServiceState.setAuthToken(token)
}

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Provider store={store} >
        <App showLogin />
      </Provider>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
