import React from 'react';
import styles from './NavBar.module.scss';
import Logo from 'assets/images/logo/Autofaktorylogo.svg'
import { Redirect, RouteComponentProps, withRouter } from 'react-router'
import Avatar from 'react-avatar';
import {
  MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarNav,
  MDBNavItem,
  MDBNavLink,
  MDBNavbarToggler,
  // MDBSideNav,
  MDBIcon,
  MDBDropdown,
  MDBDropdownToggle,
  MDBDropdownMenu,
  MDBDropdownItem,
  MDBFormInline,
  MDBContainer,
  MDBRow,
} from "mdbreact";
import { showLogin,getCities } from 'actions'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Button, Layout } from 'antd';

interface iMapDispatchToProps {
  showLogin: any
  getCities:any
}

interface iProps extends RouteComponentProps, iMapDispatchToProps {

}
interface iState {
  isOpen: boolean,
  city: string
  cities: string[]
  token: string,
  hamburger: boolean
}

class NavBar extends React.Component<iProps, iState>{
  constructor(props: iProps) {
    super(props)
    const city = localStorage.getItem("city") || ""
    const token = localStorage.getItem("token") || ""
    this.state = {
      isOpen: false,
      cities: [],
      city,
      token,
      hamburger: false
    }
  }


  setCity = (city: string) => {
    localStorage.setItem("city", city)
    if (city)
      this.setState({ city })
  }

  toggle = (nr: number) => () => {
    let modalNumber = "modal" + nr;
    // @ts-ignore 
    let val = this.state[modalNumber]
    this.setState(
      // @ts-ignore 
      {
        [modalNumber]: !val
      });
  };
  toggleCollapse = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };
  componentDidMount() {
    
      this.props.getCities().then((cities:string[])=>{
        this.setState({cities},()=>{if (!this.state.city)localStorage.setItem("city", "Mumbai")})
      })

  }
  logout() {
    console.log("logout");

    localStorage.removeItem('token');
    this.setState({ 'token': '' })
    this.props.history.push('/')
    this.props.history.go(0)

  }

  toggleHamburber() {
    this.setState({ hamburger: !this.state.hamburger })
  }
  render() {

    return (
      <>
        {this.state.hamburger && <div className={styles.sidebarOffset} onClick={e => this.setState({ hamburger: false })}></div>}
        <div className={`d-block d-lg-none ${styles.sidebarContainer}`} style={this.state.hamburger ? { left: 0 } : undefined}>
          <ul className={styles.sidebarItemContainer}>

            <MDBNavItem className={`mb-4  px-3`} >
              <a href="tel:+918888888888" className={styles.callusContainer}>
                Call Us At
                  <div className={styles.callusnumber}>(+91)88888-88888</div>
              </a>
            </MDBNavItem>
            <MDBNavItem className={styles.sidebarNavItem} active={this.props.location.pathname.includes("/buy")}>
              <MDBNavLink to={`/buy`}>Buy</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem className={styles.sidebarNavItem} active={this.props.location.pathname.includes("/sell")}>
              <MDBNavLink to="/sell">Sell</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem className={styles.sidebarNavItem} active={this.props.location.pathname.includes("/sell")}>
              <MDBNavLink to={`/aboutus`} active={this.props.location.pathname.includes("/aboutus")}>About Us</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem className={styles.sidebarNavItem} active={this.props.location.pathname.includes("/sell")}>
              <MDBNavLink to={`/howitworks`} active={this.props.location.pathname.includes("/howitworks")}>How it works</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem className={styles.sidebarNavItem} active={this.props.location.pathname.includes("/sell")}>
              <MDBNavLink to={`/blogs`} active={this.props.location.pathname.includes("/blogs")}>Blogs</MDBNavLink>
            </MDBNavItem>
            <MDBNavItem className={styles.sidebarNavItem} active={this.props.location.pathname.includes("/sell")}>
              <MDBNavLink to={`/faq`} active={this.props.location.pathname.includes("/faq")}>FAQ</MDBNavLink>
            </MDBNavItem>
          </ul>
        </div>
        <MDBNavbar className={styles.NavBar} expand="lg">
          {/* <MDBContainer fluid={window.innerWidth < 1200} > */}
            <Button icon={<i className="fas fa-bars"></i>} className={`d-lg-none d-block ${styles.hamburgerButton}`} onClick={this.toggleHamburber.bind(this)} />
            <MDBNavbarBrand>
              <MDBNavLink to="/" className="p-0">
                <object
                  className={styles.logoImg}
                  data={Logo}>

                </object>
              </MDBNavLink>
            </MDBNavbarBrand>
            <MDBNavbarNav left className="ml-xl-5 d-none d-md-block">
              <MDBNavItem className="px-1 px-lg-0 ">
                <MDBRow>
                  <MDBDropdown>
                    <MDBDropdownToggle nav caret>
                      <span className="mr-2">{this.state.city}</span>
                    </MDBDropdownToggle>
                    <MDBDropdownMenu>
                      {this.state.cities.map((e: string) => <MDBDropdownItem href="#" onClick={() => this.setCity(e)}>{e}</MDBDropdownItem>)}

                    </MDBDropdownMenu>
                  </MDBDropdown>
                  <MDBFormInline waves className="">
                    <div className="md-form my-0">
                      <input className="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search" />
                    </div>
                  </MDBFormInline>
                </MDBRow>
              </MDBNavItem>
            </MDBNavbarNav>

            <MDBNavbarNav className="d-lg-flex d-none  " right>
              <MDBNavItem className="px-1 px-lg-0" active={this.props.location.pathname.includes("/buy")}>
                <MDBNavLink to={`/buy`}>Buy</MDBNavLink>
              </MDBNavItem>
              <MDBNavItem className="px-1 px-lg-0" active={this.props.location.pathname.includes("/sell")}>
                <MDBNavLink to="/sell">Sell</MDBNavLink>
              </MDBNavItem>
              <MDBNavItem className="px-1 px-lg-0">
                <MDBDropdown>
                  <MDBDropdownToggle nav caret>
                    <span className="mr-2">More</span>
                  </MDBDropdownToggle>
                  <MDBDropdownMenu>
                    <MDBDropdownItem href={`/aboutus`} active={this.props.location.pathname.includes("/aboutus")}>About Us</MDBDropdownItem>
                    <MDBDropdownItem href={`/howitworks`} active={this.props.location.pathname.includes("/howitworks")}>How it works</MDBDropdownItem>
                    <MDBDropdownItem href={`/blogs`} active={this.props.location.pathname.includes("/blogs")}>Blogs</MDBDropdownItem>
                    <MDBDropdownItem href={`/faq`} active={this.props.location.pathname.includes("/faq")}>FAQ</MDBDropdownItem>
                  </MDBDropdownMenu>
                </MDBDropdown>
              </MDBNavItem>
              <MDBNavItem className="px-1 px-lg-0" >
                <a href="tel:+918888888888" className={styles.callusContainer}>
                  Call Us At
                  <div className={styles.callusnumber}>(+91)88888-88888</div>
                </a>
              </MDBNavItem>
            </MDBNavbarNav>

            <MDBNavbarNav right >

              {this.state.token ?
                <MDBDropdown>
                  <MDBDropdownToggle nav caret className="px-0 ml-2">
                    <Avatar name="Foo Bar" size="40" round="20px" />
                  </MDBDropdownToggle>
                  <MDBDropdownMenu>
                    <MDBDropdownItem href="#" onClick={() => this.props.history.push('/favourites')}>Favourites</MDBDropdownItem>
                    <MDBDropdownItem href="#" onClick={this.logout.bind(this)}>Logout</MDBDropdownItem>
                  </MDBDropdownMenu>
                </MDBDropdown>
                :
                // <button className="btn btn-primary">Login</button>
                <MDBIcon icon="user-alt" className={styles.signinIcon} title="Sign In" onClick={this.props.showLogin} />
              }
            </MDBNavbarNav>
            {/* <MDBNavbarToggler image={'https://mdbootstrap.com/img/svg/hamburger6.svg?color=333333'} className="float-right" onClick={this.toggleCollapse} /> */}
            <br />
            <MDBNavbarNav className="ml-5 w-100 d-block d-md-none">
              <MDBNavItem className="px-1 px-lg-0 ">
                <MDBRow center>
                  <MDBDropdown>
                    <MDBDropdownToggle nav caret>
                      <span className="mr-2">{this.state.city}</span>
                    </MDBDropdownToggle>
                    <MDBDropdownMenu>
                      {this.state.cities.map((e: string) => <MDBDropdownItem href="#" onClick={() => this.setCity(e)}>{e}</MDBDropdownItem>)}

                    </MDBDropdownMenu>
                  </MDBDropdown>
                  <MDBFormInline waves className="">
                    <div className="md-form my-0">
                      <input className="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search" />
                    </div>
                  </MDBFormInline>
                </MDBRow>
              </MDBNavItem>
            </MDBNavbarNav>

          {/* </MDBContainer> */}
        </MDBNavbar>

      </>
    );
  }
}

export default connect<null, iMapDispatchToProps>(null, { showLogin ,getCities})(withRouter(NavBar))
