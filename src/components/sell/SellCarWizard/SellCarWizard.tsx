import React from 'react';
import styles from './SellCarWizard.module.scss';
import { Steps, DatePicker, Select, Input } from 'antd';
import { MDBCard, MDBContainer } from 'mdbreact';
import { render } from '@testing-library/react';
import { string } from 'prop-types';
const { Option } = Select;

const { Step } = Steps;
interface iState {
  current: number,
  steps: any[],
  data: any
  models: string[]
}
interface iProps {

}
const makeAndModelData = {
  'Hyundai': [
    "Accent",
    "Accent Viva",
    "Aura",
    "Creta",
    "Elantra",
    "Elite i20",
    "Eon",
    "Fluidic Verna 4S",
    "Getz",
    "Getz Prime",
    "Grand i10",
    "Grand i10 Nios",
    "i10",
    "i20",
    "i20 Active",
    "Neo Fluidic Elantra",
    "New Santro 1.1",
    "Santa Fe",
    "Santro Xing",
    "Sonata",
    "Sonata Embera",
    "Sonata Transform",
    "Tucson",
    "Venue",
    "Verna",
  ],
  'Maruti Suzuki': [
    "800",
    "A-Star",
    "Alto",
    "Alto 800",
    "Alto K10",
    "Baleno",
    "Celerio",
    "Celerio X",
    "Ciaz",
    "Dzire",
    "Eeco",
    "Ertiga",
    "Esteem",
    "Estilo",
    "Grand Vitara",
    "Gypsy",
    "Ignis",
    "Kizashi",
    "New Ertiga",
    "Omni",
    "Ritz",
    "S-Cross",
    "S-Presso",
    "Swift",
    "Swift Dzire",
    "SX4",
    "Versa",
    "Vitara Brezza",
    "Wagon R",
    "Wagon R 1.0",
  ],
  'Honda': [
    "Accord",
    "Amaze",
    "BR-V",
    "Brio",
    "City",
    "Civic",
    "CR-V",
    "Jazz",
    "Mobilio",
    "New Amaze",
    "WR-V",
  ],
  'Renault': [
    "Captur",
    "Duster",
    "Fluence",
    "Koleos",
    "Kwid",
    "Lodgy",
    "Pulse",
    "Scala",
    "Triber",
  ],

  'Toyota': [
    "Camry",
    "Corolla",
    "Corolla Altis",
    "Etios",
    "Etios Cross",
    "Etios Liva",
    "Fortuner",
    "Glanza",
    "Innova",
    "Innova Crysta",
  ],
  'Ford': [
    "EcoSport",
    "Endeavour",
    "Fiesta",
    "Fiesta Classic",
    "Figo",
    "Figo Aspire",
    "Freestyle",
    "Fusion",
    "Ikon",
    "Mondeo",
  ],
  'Mahindra': [
    "Bolero",
    "e2o",
    "KUV100",
    "Marazzo",
    "Marshal",
    "NuvoSport",
    "Quanto",
    "REVAi",
    "Scorpio",
    "Scorpio Getaway",
    "Thar",
    "TUV300",
    "Verito",
    "Verito Vibe CS",
    "XUV 300",
    "XUV500",
    "Xylo",
  ],
  'Tata': [
    "Altroz",
    "Aria",
    "Bolt",
    "Harrier",
    "Hexa",
    "Indica",
    "Indica eV2",
    "Indica V2",
    "Indica Vista",
    "Indicab",
    "Indigo",
    "Indigo CS",
    "Indigo eCS",
    "Indigo Marina",
    "Indigo XL",
    "Manza",
    "Nano",
    "Nano GenX",
    "Nexon",
    "Safari",
    "Safari Storme",
    "Sumo Gold",
    "Sumo Grande",
    "Sumo Grande MK II",
    "Sumo Spacio",
    "Sumo Victa",
    "Tiago",
    "Tigor",
    "Vista Tech",
    "Zest",
  ],
};
const rtoLocations = ["Mumbai,Thane"]
const varient = {
  "Petrol": [
    "a", "b", "c"
  ],
  "Diesel": [
    "a", "b", "c"
  ]
}
const kms = [
  "a", "b", "c"
]


class SellCarWizard extends React.Component<iProps, iState>{
  constructor(props: iProps) {
    super(props)
    this.state = {
      current: 0,
      data: {
        year: null,
        make: "",
        rtoLocation: ""
      },
      steps: [],
      models: []
    }
  }
  componentDidMount() {
    this.setSteps()
  }

  setSteps() {
    const makes = Object.keys(makeAndModelData)
    const make: string = this.state.data.make || makes[0]
    //@ts-ignore
    const models: string[] = make ? makeAndModelData[make] : model
    this.setState({ models })

    const steps = [
      {
        content: <div>
          <div className={styles.formControl}>
            <div className={styles.formLabel}>Select Year</div>
            <DatePicker value={this.state.data.year} onChange={e => this.onChange(e, "year")} name="year" picker="year" />
          </div>
          <div className={styles.formControl}>
            <div className={styles.formLabel}>Select Make</div>
            <Select defaultValue={make} className={styles.formInput} style={{ width: 120 }} onChange={(e) => this.onChange(e, "make")} >
              {makes.map(e => (
                <Option key={e} value={e} >{e}</Option>
              ))}
            </Select>
          </div>
          <div className={styles.formControl}>
            <div className={styles.formLabel}>Select Model</div>
            <Select defaultValue={models[0]} style={{ width: 120 }} onChange={(e) => this.onChange(e, "model")}>
              {models.map(e => (
                <Option key={e} value={e} >{e}</Option>
              ))}
            </Select>
          </div>
          <div className={styles.formControl}>
            <div className={styles.formLabel}>Select RTO Location</div>
            <Select style={{ width: 120 }} onChange={(e) => this.onChange(e, "rtoLocation")}>
              {rtoLocations.map(e => (
                <Option key={e} value={e} >{e}</Option>
              ))}
            </Select>
          </div>
        </div>,
        title: "Basic Data",
        description:"Tell us about your car..."
      },
      {
        content: <div>
          <div className={styles.formControl}>
            <div className={styles.formLabel}>Fuel Type</div>
            <Select style={{ width: 120 }} onChange={(e) => this.onChange(e, "fuelType")}>
              {rtoLocations.map(e => (
                <Option key={e} value={e} >{e}</Option>
              ))}
            </Select>
          </div>
          <div className={styles.formControl}>
            <div className={styles.formLabel}>Varient</div>
            <Select style={{ width: 120 }} onChange={(e) => this.onChange(e, "rtoLocation")}>
              {/* @ts-ignore */}
              {this.state.data.fuelType && varient[this.state.data.fuelType].map(e => (
                <Option key={e} value={e} >{e}</Option>
              ))}
            </Select>
          </div>
          <div className={styles.formControl}>
            <div className={styles.formLabel}>KM Driven</div>
            <Select style={{ width: 120 }} onChange={(e) => this.onChange(e, "km")}>
              {kms.map(e => (
                <Option key={e} value={e} >{e}</Option>
              ))}
            </Select>
          </div>
        </div>,
        title: "Additional Info",
        description: "Any other information you would like to share..."
      },
      {
        title: "Contact Info",
        description: "Tell us about you...",
        content: <>

          <div className={styles.formControl}>
            <div className={styles.formLabel}>Name</div>
            <Input value={this.state.data.name} onChange={(e) => this.onChange(e, "name")} />
          </div>

          <div className={styles.formControl}>
            <div className={styles.formLabel}>Available Time </div>
            <Input value={this.state.data.name} onChange={(e) => this.onChange(e, "availability")} />
          </div>
        </>
      }
    ]

    this.setState({ steps })
  }

  onChange(val: any, key: string) {

  }

  next() {
    const { current } = this.state
    this.setState({ current: current + 1 })
  }

  prev() {

    const { current } = this.state
    this.setState({ current: current - 1 })
  }

  render() {
    const { current, steps } = this.state
    return (
      <MDBContainer className={styles.SellCarWizard}>
        <MDBCard className="p-5">
          <Steps  current={this.state.current} className={styles.steps} >
            {this.state.steps.map((e, i) => <Step subTitle={e.title}  />)}
          </Steps>
          {this.state.steps[this.state.current] &&
            <div className={styles.stepData}>
              <h3 className={styles.stepTitle}>{this.state.steps[this.state.current].description}</h3>

              {this.state.steps[this.state.current].content}
            </div>
          }
          <div className={styles.stepsection}>
            {current < steps.length - 1 && (
              <button className="btn btn-primary" onClick={this.next.bind(this)}>
                Next
              </button>
            )}
            {current === steps.length - 1 && (
              <button className="btn btn-primary" onClick={() => alert('Processing complete!')}>
                Done
              </button>
            )}
            {current > 0 && (
              <button className="btn btn-disabled" onClick={this.prev.bind(this)}>
                Previous
              </button>
            )}
          </div>
        </MDBCard>
      </MDBContainer>
    );
  }
}

export default SellCarWizard;
