import { MDBCol, MDBContainer, MDBRow } from 'mdbreact';
import React from 'react';
import styles from './HeroComponent.module.scss';
import backgroundImage from 'assets/images/Imagesofteam/car14.jpg'

interface iProps {

}
interface iState {
  makeList:string[],
  modelList:string[],
  model:string,
  make:string
}


class HeroComponent extends React.Component<iProps, iState>{
  constructor(props:iProps){
    super(props)
    this.state={
      makeList:[],
      modelList:[],
      model:"",
      make:""
    }
  }
  handleChange=(e:any)=> {
  }

  render() {
    return(
      <div className={styles.HeroComponent} style={{ backgroundImage: `url(${backgroundImage})` }}>
        <MDBContainer className={styles.HeroComponentContainer}>
          <div className={styles.TopHead}>
            Buy A Car You Will Love. Guarenteed.
      </div>
          <div className={styles.SubHead}>
            Highest quality used cars | Free waranty | 5 day money back guarenteed.
      </div>
        </MDBContainer>
        <MDBContainer className={styles.SelectRowContainer}>
          <MDBRow  center>
            <MDBCol lg="3" sm="6" md="4" xs="8" className="mb-2">
              <select
                className="browser-default custom-select"
                name="make"
                onChange={this.handleChange}
              >
                <option selected hidden >Select Make</option>

              </select>
            </MDBCol>
            <MDBCol lg="3" sm="6" md="4" xs="8" className="mb-2">
              <select
                className="browser-default custom-select"
                name="model"
                onChange={this.handleChange}
              >
                <option selected hidden >Select Model</option>

              </select>
            </MDBCol>

              <button className="mt-0 mb-2 px-4 btn btn-secondary  "  >
                Search car
              </button>
          </MDBRow>
        </MDBContainer>

      </div>
    );
  }
}
export default HeroComponent;
