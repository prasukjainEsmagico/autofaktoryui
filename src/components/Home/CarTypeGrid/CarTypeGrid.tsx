import { MDBCard, MDBCol, MDBContainer, MDBIcon, MDBRow } from 'mdbreact';
import React from 'react';
import styles from './CarTypeGrid.module.scss';
import SampleImage from "assets/images/homepage/carCategory/hatchback.webp"
import AllCarsImage from "assets/images/homepage/carCategory/allCars.jpeg"

const categories = [
  {
    name: "HatchBack",
    link: "/buy?cartype=hatchback",
    imageUrl: SampleImage
  },
  {
    name: "HatchBack",
    link: "/buy?cartype=hatchback",
    imageUrl: SampleImage
  },
  {
    name: "HatchBack",
    link: "/buy?cartype=hatchback",
    imageUrl: SampleImage
  },
  {
    name: "HatchBack",
    link: "/buy?cartype=hatchback",
    imageUrl: SampleImage
  },
  {
    name: "HatchBack",
    link: "/buy?cartype=hatchback",
    imageUrl: SampleImage
  },
  {
    name: "HatchBack",
    link: "/buy?cartype=hatchback",
    imageUrl: SampleImage
  },
  {
    name: "HatchBack",
    link: "/buy?cartype=hatchback",
    imageUrl: SampleImage
  },

]
interface iProps {
  // categories:any
}
const CarTypeGrid: React.FC<iProps> = (props: iProps) => (
  <div className={styles.CarTypeGridContainer}>
    <MDBContainer>
      <h2 className="h1-responsive text-center font-weight-bold mb-5 text-dark" >
        Select Car By Categories
      </h2>
      <MDBRow >
        {categories.map(e => (
          <MDBCol size="6" xs="5" md="3" className="text-center mb-5">
            <MDBCard className={styles.card}>
              <img src={e.imageUrl} className={styles.cardImage} />
              <div className={styles.gradient}>
                <p className={styles.cardText}>{e.name}</p>
              </div>
            </MDBCard>
          </MDBCol>
        ))}
        <MDBCol size="6" xs="5" md="3" >
          <MDBCard className={styles.card}>
            <img src={AllCarsImage} className={styles.cardImage} />
            <div className={styles.gradient}></div>
            <div className={styles.gradient2}>
              <p className={styles.allCars}>
              Show All Cars<br/>
              <MDBIcon icon="arrow-right"/>
              </p>
            </div>
          </MDBCard>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  </div>
);

export default CarTypeGrid;
