import { MDBCol, MDBContainer, MDBRow } from 'mdbreact';
import React from 'react';
import { Link } from 'react-router-dom';
import styles from './HomeCarFilters.module.scss';

const categories = [
  {
    name: "Maruti Suzuki Alto 800",
    link: "/buy/maruti suzuki/alto 800",
  },
  {
    name: "Maruti Suzuki Alto 800",
    link: "/buy/maruti suzuki/alto 800",
  },
  {
    name: "Maruti Suzuki Alto 800",
    link: "/buy/maruti suzuki/alto 800",
  },
  {
    name: "Maruti Suzuki Alto 800",
    link: "/buy/maruti suzuki/alto 800",
  },
  {
    name: "Maruti Suzuki Alto 800",
    link: "/buy/maruti suzuki/alto 800",
  },
  {
    name: "Maruti Suzuki Alto 800",
    link: "/buy/maruti suzuki/alto 800",
  },
  {
    name: "Maruti Suzuki Alto 800",
    link: "/buy/maruti suzuki/alto 800",
  },
  {
    name: "Maruti Suzuki Alto 800",
    link: "/buy/maruti suzuki/alto 800",
  },
  {
    name: "Maruti Suzuki Alto 800",
    link: "/buy/maruti suzuki/alto 800",
  },
  {
    name: "Maruti Suzuki Alto 800",
    link: "/buy/maruti suzuki/alto 800",
  },
  {
    name: "Maruti Suzuki Alto 800",
    link: "/buy/maruti suzuki/alto 800",
  },
  {
    name: "Maruti Suzuki Alto 800",
    link: "/buy/maruti suzuki/alto 800",
  },
  {
    name: "Maruti Suzuki Alto 800",
    link: "/buy/maruti suzuki/alto 800",
  },
  {
    name: "Maruti Suzuki Alto 800",
    link: "/buy/maruti suzuki/alto 800",
  },
  {
    name: "Maruti Suzuki Alto 800",
    link: "/buy/maruti suzuki/alto 800",
  },
  {
    name: "Maruti Suzuki Alto 800",
    link: "/buy/maruti suzuki/alto 800",
  },
  {
    name: "Maruti Suzuki Alto 800",
    link: "/buy/maruti suzuki/alto 800",
  },
  {
    name: "Maruti Suzuki Alto 800",
    link: "/buy/maruti suzuki/alto 800",
  },

]
const HomeCarFilters: React.FC = () => (
  <div className={styles.HomeCarFilters}>
    <MDBContainer>
      <h2 className="h1-responsive text-center font-weight-bold mb-5 text-dark" >
        Choose A Car Of Your Choice
      </h2>
      <MDBRow >
        {categories.map(e => (
          <MDBCol size="12" sm="6" md="4"  className="text-center mb-3">
            <Link to={e.link} className={styles.link}>{e.name}</Link>
          </MDBCol>
        ))}
        <div className="w-100 text-center mb-5">
          <button className="btn btn-secondary">Search all cars</button>
        </div>
      </MDBRow>
    </MDBContainer>
  </div>
);

export default HomeCarFilters;
