import { Button, Carousel, DatePicker, Input, message, Select } from 'antd';
import Modal from 'antd/lib/modal/Modal';
import React from 'react';
import styles from './TestDriveModal.module.scss';
import { CloseOutlined } from '@ant-design/icons'
import { connect } from 'react-redux';
import {getOtp,sendTestDriveData} from 'actions'
import { Link, Redirect, RouteComponentProps, withRouter } from 'react-router-dom';

const { Option } = Select

interface iState {
  otp: string
  time_slot: string
  date: any,
  phone: string
  error : any
  loading:boolean
}


interface iMapStateToProps {
  // Your properties here
}

interface iMapDispatchToProps {
  // Your properties here
  getOtp:any,
  sendTestDriveData:any
}
interface iOwnProps {
  onCloseModal: any,
  car: number | string,
}
interface iProps extends iMapDispatchToProps, iMapStateToProps, RouteComponentProps, iOwnProps {
}

class TestDriveModal extends React.Component<iProps, iState>{
  constructor(props: iProps) {
    super(props)
    this.state = {
      otp: "",
      time_slot: "",
      date: null,
      phone: "",
      loading:false,
      error : {
        otp: false,
        time_slot: false,
        date: false,
        phone: false,
      }
    }

  }


  private regex = {
    otp: /^[\d]{4,6}$/,
    time_slot: /^.{1,}$/,
    phone: /^[\d]{10}$/,
  }


  private carousel: any = React.createRef<typeof Carousel>()
  componentDidMount() {
    // this.responseGoogle()
  }

  onChange(val: any, key: string) {
    // @ts-ignore
    this.setState({[key]:val})
  }

  resendOTP(){

  }


  setTimeInfo() {    
    if( !this.state.date)
      return this.setState({error:{...this.state.error,date:true}})
    if (! this.regex.time_slot.test(this.state.time_slot))
      return this.setState({error:{...this.state.error,time_slot:true}})
    this.setState({error : {
      otp: false,
      time_slot: false,
      date: false,
      phone: false,
    }})
    this.carousel.next()
  }
  getOTP() {
    if (! this.regex.phone.test(this.state.phone))
      return this.setState({error:{...this.state.error,phone:true}})
    this.setState({loading:true})
    this.props.getOtp(this.state.phone)
    .then(()=>this.setState({loading:false},()=>this.carousel.next()))
    .catch((e:Error)=>{
      message.error(e.message)
      this.setState({loading:false,error:{...this.state.error,phone:true}})})
  
  }
  submitData() {
    let {date,time_slot,phone,otp}=this.state
    if (! this.regex.otp.test(otp))
      return this.setState({error:{...this.state.error,otp:true}})
    this.setState({loading:true})
    let data = {date,time_slot,otp,car:this.props.car}
    this.props.sendTestDriveData(phone,data)
    .then(()=>this.carousel.next())
    .catch((e:Error)=>{
      message.error(e.message)
      this.setState({loading:false,error:{...this.state.error,otp:true}})})
  
  }


  render() {
    // const {loading, } = this.state;
    const timeslotList = ["9am to 11am", "11am to 1pm", "1pm to 3pm", "3pm to 5pm", "5pm to 7pm"]

    return (
      <Modal
        visible={true}
        title={null}
        onCancel={this.handleCancel.bind(this)}
        footer={null}
        maskStyle={{
          background: " #00000099",
          mixBlendMode: "normal",
        }}
        cancelButtonProps={{
          className: styles.cancelButton
        }}
        className={styles.TestDriveModal}
        destroyOnClose={true}
        wrapClassName={styles.wrapClassName}
        closeIcon={<CloseOutlined color="#ffffff" />}
        bodyStyle={{

          background: "#e9e7e7",
          border: "1px solid #B9B9B9",
          boxSizing: "border-box",
          borderRadius: "20px",
          padding: "72px 96px",
        }}
      >
        <Carousel dots={false} ref={node => (this.carousel = node)} >
          <div className="text-left">
          <div className={styles.sectionHead}>Please provide your confortable time.</div>
            <div className={styles.formControl}>
              <div className={styles.formLabel}>Select Date</div>
              <DatePicker size="large" className={styles.formInput} style={{borderColor: this.state.error.date && 'red'}} onChange={e => this.onChange(e?.toDate(), "date")} name="date" picker="date" />
              {this.state.error.date && <div className={styles.errorMessage}>Please select date.</div>}
            </div>
            <div className={styles.formControl}>
              <div className={styles.formLabel}>Select TimeSlot</div>
              <Select size="large" defaultValue={this.state.time_slot} className={styles.formInput} style={{borderColor: this.state.error.time_slot && 'red'}} onChange={(e) => this.onChange(e, "time_slot")} >
                {timeslotList.map(e => (
                  <Option key={e} value={e} >{e}</Option>
                ))}
              </Select>
              {this.state.error.time_slot && <div className={styles.errorMessage}>Please select time slot.</div>}
            </div>
            <Button loading={this.state.loading} className="btn btn-primary mt-2" onClick={this.setTimeInfo.bind(this)}>Next</Button>
          </div>
          <div>
            <div className={styles.sectionHead}>Please provide your mobile Number.</div>
            <div className={styles.formControl}>
              <Input style={{ borderColor: this.state.error.phone && 'red' }} size="large" addonBefore={"+91"} value={this.state.phone} onChange={(e) => this.onChange(e.target.value, "phone")} placeholder="Phone" />
              {this.state.error.phone && <div className={styles.errorMessage}>Enter valid 10 digit phone number.</div>}
            </div>
            <Button loading={this.state.loading} className="btn btn-primary mt-2" onClick={this.getOTP.bind(this)}>Get OTP</Button>
          </div>
          <div>

            <div className={styles.sectionHead}>Please enter OTP.</div>
            <div className={styles.formControl}>
              <Input style={{ borderColor: this.state.error.otp && 'red' }} size="large" value={this.state.otp} onChange={(e) => this.onChange(e.target.value, "otp")} />
              {this.state.error.otp && <div className={styles.errorMessage}>Enter valid OTP.</div>}
              {/* {setTimeout(()=> (<a onClick={this.resendOTP.bind(this)} className={styles.errorMessage}>Enter valid 10 digit phone number.</a>),100)} */}
            </div>
            <Button loading={this.state.loading} className="btn btn-primary mt-2" onClick={this.submitData.bind(this)}>Submit</Button>

          </div>
          <div>
            <h4 className="text-center">Thank you for showing interest in the car. Your information has been submitted. We will contact you shortly for further details. </h4>
          </div>
        </Carousel>
      </Modal >
    );
  }
  handleCancel(e: any = null) {
    this.props.onCloseModal(false)

  }
}

function mapStateToProps(state: any) {
  return {
  }
}

export default connect<iMapStateToProps, iMapDispatchToProps, iOwnProps>(mapStateToProps, {getOtp,sendTestDriveData})(withRouter(TestDriveModal))
