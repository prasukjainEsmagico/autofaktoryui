import { Spin } from 'antd';
import React from 'react';
import styles from './Loader.module.scss';

const Loader: React.FC = () => (
  <div className={styles.LoaderOffset}>
  <div className={styles.Loader}>

        <Spin size="large" />
        <br/>
        <p className={styles.LoaderText}>Please wait while we ready up the Car</p>
</div>
  </div>
);

export default Loader;
