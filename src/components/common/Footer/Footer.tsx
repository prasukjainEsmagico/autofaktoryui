import { MDBCol, MDBContainer, MDBRow } from 'mdbreact';
import React from 'react';
import { Link } from 'react-router-dom';
import styles from './Footer.module.scss';

const Footer: React.FC = () => (
  <footer className={`page-footer pt-4 ${styles.Footer}`}>
    <MDBContainer >
      <MDBRow  className="flex-row-reverse">
        
        <MDBCol  >
          <h6 className="text-uppercase mb-4 font-weight-bold">Contact</h6>
          <p className="footertext">
            <i className="fas fa-home mr-3"></i> Get instant Quotes</p>
          <p className="footertext">
            {/* <p className="footertext">
                      <i className="fas fa-home mr-3"></i> Banglore Gorl's Campus</p>
                    <p className="footertext"></p> */}
            <i className="fas fa-envelope mr-3"></i> test@gmail.com </p>
          <p className="footertext">
            <i className="fas fa-phone mr-3"></i> Call us on  - 72772 77275</p>
          <ul className="list-unstyled list-inline">
            <li className="mb-3">
              <a className="" href="https://www.instagram.com/">
              <i className="fab fa-instagram mr-4"></i>
                Instagram 
              </a>
            </li>
            {/* <li className="mb-3">
                          <a className="" href="https://www.instagram.com/navgurukul.dharamsala/">
                            Banglore <i className="fab fa-instagram"></i>
                          </a>
                        </li> */}
            <li className="mb-3">
              <i className="fab fa-facebook-f mr-4"></i>
              <a className="" href="https://www.facebook.com/">
                facebook 
              </a>
            </li>
            <li className="mb-3">
              <a className="" href="https://www.youtube.com/">
              <i className="fab fa-youtube mr-4"></i>Youtube 
              </a>
            </li>
          </ul>
        </MDBCol>
        <div className="col">
          <h6 className="text-uppercase mb-4 font-weight-bold">Navigations</h6>
          <ul className="list-unstyled list-inline">
            <li><Link to="/buy">Buy Cars</Link></li>
            <li><Link to="/sell">Sell Cars</Link></li>
            <li><Link to="/blog">Blogs</Link></li>
            <li><Link to="/faq">FAQ</Link></li>
            <li><Link to="/howitworks">How it works</Link></li>
            <li><Link to="/terms">Terms of Use</Link></li>
            <li><Link to="/legal">Legal Disclaimer</Link></li>
            <li><Link to="/about">About us</Link></li>
            <li><Link to="/caeers">Careers</Link></li>
          </ul>
        </div>
        <MDBCol size="12" md="4"  >
          <h6 className="text-uppercase mb-4 font-weight-bold">AutoFaktory</h6>
          <p className="footertext">
            AutoFaktory is the most trusted way of buying & selling used cars. Choose from over 1000+ fully inspected second hand cars models. Select online and book a test drive at a AutoFaktory Car Hub near you. Get 5 days assured money back guarantee and free 1 year extended warranty on all AutoFaktory Assured pre owned cars.</p>
        </MDBCol> 
      </MDBRow>
      <div className="text-center">

      </div>
      <hr />
      <div>
        <div>
          <p className="text-center mb-0  pb-2">© 2020 AutoFaktory.com </p>
        </div>
      </div>
    </MDBContainer>
  </footer>
);

export default Footer;
