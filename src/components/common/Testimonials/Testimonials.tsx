import { MDBCard, MDBCardImage, MDBCol, MDBContainer, MDBMask, MDBRow, MDBView } from 'mdbreact';
import React from 'react';
import Carousel from 'react-multi-carousel';
import { Link } from 'react-router-dom';
import styles from './Testimonials.module.scss';
import SampleImage from "assets/images/LoveStory/testimonial-1.jpg"

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
    slidesToSlide: 3 // optional, default to 1.
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
    slidesToSlide: 2 // optional, default to 1.
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    slidesToSlide: 1 // optional, default to 1.
  }
};

const testimonials = [
  {
    img: SampleImage,
    make: "Maruti Suzuki",
    model: "Alto 800",
    name:"Vandana Arora",
    city:"Indore"
  },
  {
    img: SampleImage,
    make: "Maruti Suzuki",
    model: "Alto 800",
    name:"Vandana Arora",
    city:"Indore"
  },
  {
    img: SampleImage,
    make: "Maruti Suzuki",
    model: "Alto 800",
    name:"Vandana Arora",
    city:"Indore"
  },
  {
    img: SampleImage,
    make: "Maruti Suzuki",
    model: "Alto 800",
    year: "2018",
    id: 123,
    kms: 150000,
    fuel: "Petrol",
    bodyType: "HatchBack",
    transmission: "Manual",
    price: "2500000"
  },
  {
    img: SampleImage,
    make: "Maruti Suzuki",
    model: "Alto 800",
    name:"Vandana Arora",
    city:"Indore"
  },
  {
    img: SampleImage,
    make: "Maruti Suzuki",
    model: "Alto 800",
    name:"Vandana Arora",
    city:"Indore"
  },
]
const Testimonials: React.FC = () => (
  <div className={styles.Testimonials}>
    <MDBContainer>
      <h2 className="h1-responsive text-center my-4 text-dark">Recommended Cars  </h2>
      <Carousel
        swipeable={true}
        draggable={true}
        showDots={true}
        responsive={responsive}
        infinite={true}
        autoPlay={true}
        keyBoardControl={true}
        transitionDuration={3000}
        containerClass="carousel-container"
        removeArrowOnDeviceType={["tablet", "mobile"]}
        dotListClass="custom-dot-list-style"
        itemClass="carousel-item-padding-40-px"
      >
        {testimonials.map(e => (
          <MDBCard className={styles.card}>
            <MDBView className="overlay z-depth-1" waves>
              <img
                src={e.img}
                alt=""
                className={styles.cardImage}
              />
              <a href="#!">
                <MDBMask overlay="white-slight" />
              </a>
            </MDBView>
            <div className="content-section p-2">
              <div className="top-section">
                "Had been postponing the decision to get a car. Couldn't take a chance with my or my son's health. The car delivered at my home and the entire process was completely contactless."
                            </div>
              <div className="bottom-section text-right">
                <div className="person-name bold">- {e.name}, {e.city}</div>
                <div className="car-name bold"> {e.make} {e.model}</div>

              </div>
            </div>
          </MDBCard>
        ))}

      </Carousel>
    </MDBContainer>
  </div>
);

export default Testimonials;
