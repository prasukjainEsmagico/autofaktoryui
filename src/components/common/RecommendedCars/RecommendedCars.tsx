import { MDBCard, MDBCol, MDBContainer, MDBRow } from 'mdbreact';
import React from 'react';
import Carousel from 'react-multi-carousel';
import styles from './RecommendedCars.module.scss';
import SampleImage from "assets/images/homepage/carCategory/hatchback.webp"
import { Link } from 'react-router-dom';

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
    slidesToSlide: 3 // optional, default to 1.
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
    slidesToSlide: 2 // optional, default to 1.
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    slidesToSlide: 1 // optional, default to 1.
  }
};
const recommendedCars = [
  {
    img: SampleImage,
    make: "Maruti Suzuki",
    model: "Alto 800",
    year: "2018",
    id: 123,
    kms: 8000,
    fuel: "Petrol",
    bodyType: "HatchBack",
    transmission: "Automatic",
    price: "2500000"
  },
  {
    img: SampleImage,
    make: "Maruti Suzuki",
    model: "Alto 800",
    year: "2018",
    id: 123,
    kms: 8000,
    fuel: "Petrol",
    bodyType: "HatchBack",
    transmission: "Automatic",
    price: "2500000"
  },
  {
    img: SampleImage,
    make: "Maruti Suzuki",
    model: "Alto 800",
    year: "2018",
    id: 123,
    kms: 8000,
    fuel: "Petrol",
    bodyType: "HatchBack",
    transmission: "Automatic",
    price: "2500000"
  },
  {
    img: SampleImage,
    make: "Maruti Suzuki",
    model: "Alto 800",
    year: "2018",
    id: 123,
    kms: 150000,
    fuel: "Petrol",
    bodyType: "HatchBack",
    transmission: "Manual",
    price: "2500000"
  },
  {
    img: SampleImage,
    make: "Maruti Suzuki",
    model: "Alto 800",
    year: "2018",
    id: 123,
    kms: 8000,
    fuel: "Petrol",
    bodyType: "HatchBack",
    transmission: "Automatic",
    price: "2500000"
  },
  {
    img: SampleImage,
    make: "Maruti Suzuki",
    model: "Alto 800",
    year: "2018",
    id: 123,
    kms: 8000,
    fuel: "Petrol",
    bodyType: "HatchBack",
    transmission: "Automatic",
    price: "2500000"
  },
]
const RecommendedCars: React.FC = () => (
  <div className={styles.RecommendedCars}>
    <MDBContainer>
    <h2 className="h1-responsive text-center my-4 text-dark">Recommended Cars  </h2>
      <Carousel
        swipeable={true}
        draggable={true}
        showDots={true}
        responsive={responsive}
        infinite={true}
        autoPlay={true}
        keyBoardControl={true}
        transitionDuration={3000}
        containerClass="carousel-container"
        removeArrowOnDeviceType={["tablet", "mobile"]}
        dotListClass="custom-dot-list-style"
        itemClass="carousel-item-padding-40-px"
      >
        {recommendedCars.map(e => (
          <MDBCard className={styles.card}>
            <img src={e.img} className={styles.cardImage} />
            <div className={styles.gradient}></div>
            <div className={styles.gradient2}>
              <h3 className={styles.carName}>{e.make} {e.model}</h3>
              <div className={styles.carContent}>
                <MDBRow between>
                  <MDBCol size="6" className="text-left"><span className={styles.labels}>{e.kms} KMs</span></MDBCol>
                  <MDBCol size="6" className="text-right pr-5"><span className={styles.labels}>Rgd.{e.year}</span></MDBCol>
                  <MDBCol size="6" className="text-left"><span className={styles.labels}>{e.fuel}</span></MDBCol>
                  <MDBCol size="6" className="text-right pr-5"><span className={styles.labels}>{e.transmission}</span></MDBCol>
                </MDBRow>
                <MDBRow between>
                  <span className={styles.price}>&#8377;{e.price}</span>
                  <Link to={`details/${e.id}`} className={`btn  ${styles.viewButton}`}>View</Link>
                </MDBRow>
              </div>
            </div>
          </MDBCard>
        ))}

      </Carousel>
    </MDBContainer>
  </div>
);

export default RecommendedCars;
