import { Card, Col, Row, Select, Table, Tag } from 'antd';
import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import styles from './CarsPagination.module.scss';
import emptyCars from 'assets/images/empty.jpg'
const { Option } = Select
const {Meta} = Card
interface iMapStateToProps {
  cars: any[],
  count: number
}

interface iOwnProps {

}
interface iProps extends Partial<iMapStateToProps>, iOwnProps, RouteComponentProps {
}
interface iState {

}

class CarsPagination extends React.Component<iProps, iState>{
  constructor(props: iProps) {
    super(props)
  }

  handleOrderChange(e: any) {
    const sort_order = e.target.value
    let filters = this.getFilters()
    let arr = sort_order.split('_')
    filters.sort = arr[0]
    filters.order = arr[1]
    let search = this.updateFilters(filters)

    this.props.history.push({ pathname: "/buy", search })

  }


  updateFilters(filters: any) {
    let updateFilters = { ...filters }

    if (filters.make)
      updateFilters.make = filters.make.join(',')
    if (filters.model)
      updateFilters.model = filters.model.join(',')

    return new URLSearchParams(updateFilters).toString()

  }
  getFilters() {

    let filters: any = Object.fromEntries(new URLSearchParams(this.props.location.search))
    if (filters.make) {
      filters.make = filters.make.split(',')
    }
    if (filters.model) {
      filters.model = filters.model.split(',')
    }
    return filters
  }
  render() {
    let filters = this.getFilters()
    if (filters.sort && filters.order) {
      filters.sort_order = filters.sort + '_' + filters.order
    }
    else if (filters.sort) {
      filters.sort_order = filters.sort + '_desc'
    }

    else if (filters.order) {
      filters.sort_order = '_' + filters.order
    }
    else {
      filters.sort_order = 'points_desc'

    }
    const city = localStorage.getItem('city')
    return (
      <Col flex="1" className={styles.CarsPagination}>
        <Row className={styles.topRow}>
          <Col flex="1" className={styles.carsCountCol}>
            Found {this.props.count} cars in {city} area.
          </Col>
          <Select value={filters.sort_order} style={{ width: 120 }} onChange={this.handleOrderChange.bind(this)}>
            <Option value="points_desc">Relevance</Option>
            <Option value="_desc">Newest</Option>
            <Option value="price_desc">Price: High to Low</Option>
            <Option value="price_asc">Price: Low to High</Option>
            <Option value="kms_asc">KiloMeters Driven: High to Low</Option>
            <Option value="kms_desc">KiloMeters Driven: Low to High</Option>
            <Option value="year_desc">Year: High to Low</Option>
            <Option value="year_asc">Year: Low to High</Option>
          </Select>
        </Row>
        {
          this.props.count === 0 ? <div>
            <h3 className={styles.emptyCarsText}>Garage Empty</h3>
            <img src={emptyCars} className={styles.emptyCars} />
          </div> :
            <Row>
              {this.props.cars?.map(e => <Card
                hoverable
                className="col-sm-12 col-lg-4"
                cover={<img alt="example" style={{width:"100%"}} src={e.url} />}
                onClick={()=>this.props.history.push(`/details/${e.id}`)}
              >
                <Meta title={<div>
                  <h3>{e.make} {e.model__model}</h3>
                  <h4>{e.variant__name}</h4>
                </div>} description={
                  <div>
                    <Tag>{e.km}kms</Tag>
                    <Tag>{e.registrationYear} Rgtd</Tag>
                    <Tag>{e.transmission}</Tag>
                    <Tag color={e.color}>{e.color}</Tag>
                  <Row justify="end">
                    <Col md="8" xs="12"><h4 className="bold text-right">&#8377;{e.price}</h4> </Col>
                  </Row>
                  </div>
                } />
              </Card>)}
            </Row>
        }
      </Col>
    );
  }
}


function mapStateToProps(state: any) {
  return {
    cars: state.buyCar.cars,
    count: state.buyCar.count
  }
}

export default connect<iMapStateToProps, null>(mapStateToProps, null)(withRouter(CarsPagination));



