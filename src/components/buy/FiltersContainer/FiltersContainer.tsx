import { Button, Card, Checkbox, Col, Collapse, Radio, Row, Select, Slider } from 'antd';
import { MDBCol, MDBDropdown, MDBDropdownItem, MDBDropdownMenu, MDBDropdownToggle, MDBIcon } from 'mdbreact';
import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import styles from './FiltersContainer.module.scss';
import { getFilters,getBuyCarsData } from 'actions'
import { iFilterReducer } from 'common/interfaces/interfaces';

const { Panel } = Collapse;
const { Option } = Select

interface iMapDispatchToProps {
  getFilters: any,
  getBuyCarsData:any
}

interface iMapStateToProps extends Partial<iFilterReducer> {
}
interface iProps extends iMapStateToProps, RouteComponentProps, iMapDispatchToProps {
}
interface iState {

}


class FiltersContainer extends React.Component<iProps, iState> {
  constructor(props: any) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
    let search = this.props.location.search
    this.props.getFilters().then(()=>this.props.getBuyCarsData(search))
  }


  clearFilter() {
    const { history } = this.props

    // history.push({ pathname: "/buy" })
    this.props.getBuyCarsData('')
    history.replace('/buy');
    // history.go(0);
  }


  onSelectFilter(key: string, e: any) {
    console.log(e);

    let filters = this.getFilters()
    if (!e) {
      delete filters[key]
    }
    else
      filters[key] = e
    let search = this.updateFilters(filters)

    this.props.history.push({ pathname: "/buy", search })
    this.props.getBuyCarsData(search)

  }

  // onSelectModel(model: number) {

  //   let filters = this.getFilters()
  //   if (!filters.model) {
  //     filters.model = [model]
  //   }
  //   else {
  //     let i = filters.model.indexOf(model.toString())
  //     if (i === -1)
  //       filters.model.push(model)
  //     else
  //       filters.model.splice(i, 1)
  //   }
  //   let search = this.updateFilters(filters)

  //   this.props.history.push({ pathname: "/buy", search })



  // }

  updateFilters(filters: any) {
    let updateFilters = { ...filters }

    return new URLSearchParams(updateFilters).toString()

  }
  getFilters() {

    let filters: any = Object.fromEntries(new URLSearchParams(this.props.location.search))

    return filters
  }

  render() {
    let PriceRanges = []
    let maxPrice = this.props.maxPrice || 100000
    for (let i = 1; i <= 9; i++) {
      PriceRanges.push(Math.round((maxPrice * i) / 10))
    }


    const filters: any = this.getFilters()


    const innerContent = <>
      <Row className={` row  p-4 `}>
        <Col flex="1" className={`font-weight-bold h4`} > {window.innerWidth >= 992 ? "Filters" : ""}</Col>
        {Object.keys(filters).length > 0 ? <Button className={styles.ClearButton} onClick={this.clearFilter.bind(this)}> <MDBIcon icon="undo" /> &nbsp;Clear All</Button> : ""}
      </Row>
      <Collapse bordered={false} >
        <Panel key="budget" className={styles.filterSection} header={<div className={styles.sectionHead}>Budget</div>}>
          <div className="pl-5">

            <div className={filters.label}>Price From</div>
            <Select value={filters.minPrice} style={{ width: "100%" }} key="minPrice" size="large" onChange={e => this.onSelectFilter("minPrice", e)} placeholder="Select">
              <Option value="">&#8377;0</Option>
              {PriceRanges.map((e: number) => <Option value={e}>&#8377;{e}</Option>)}
            </Select>
            <div className={filters.label}>Price To</div>
            <Select value={filters.maxPrice} style={{ width: "100%" }} key="maxPrice" size="large" onChange={e => this.onSelectFilter("maxPrice", e)} placeholder="Select">
              {PriceRanges.map((e: number) => <Option value={e}>&#8377;{e}</Option>)}
              <Option value="">Maximum</Option>
            </Select>

          </div>
        </Panel>

        <Panel key="make" className={styles.filterSection} header={<div className={styles.sectionHead}>Make and Model</div>}>

          <div className="pl-5">

            <div className={filters.label}>Make</div>
            <Select value={filters.make} style={{ width: "100%" }} key="make" size="large" onChange={e => this.onSelectFilter("make", e)} placeholder="Select">
              {Object.keys(this.props.MakeModelData).map((e: string) => <Option value={e}>{e}</Option>)}
            </Select>
            <div className={filters.label}>Model</div>
            <Select value={filters.model} style={{ width: "100%" }} key="model" size="large" onChange={e => this.onSelectFilter("model", e)} placeholder="Select">
              {filters.make && this.props.MakeModelData[filters.make] && <>
                {this.props.MakeModelData[filters.make].map((e: any) => <Option value={e.id}>{e.name}</Option>)}
              </>
              }
            </Select>

          </div>
        </Panel>

        <Panel key="year" className={styles.filterSection} header={<div className={styles.sectionHead}>Year</div>}>

          <div className="pl-5">

            <div className={filters.label}>Year From</div>
            <Select value={filters.minyear} style={{ width: "100%" }} key="minyear" size="large" onChange={e => this.onSelectFilter("minyear", e)} placeholder="Select">
              <Option value="">Any</Option>
              {this.props.year && this.props.year.map((e: number) => <Option value={e}>{e}</Option>)}
            </Select>
            <div className={filters.label}>Year To</div>
            <Select value={filters.maxyear} style={{ width: "100%" }} key="maxyear" size="large" onChange={e => this.onSelectFilter("maxyear", e)} placeholder="Select">
              {this.props.year && this.props.year.map((e: number) => <Option value={e}>{e}</Option>)}
              <Option value="">Any</Option>
            </Select>


          </div>
        </Panel>


        <Panel key="RTO" className={styles.filterSection} header={<div className={styles.sectionHead}>RTO</div>}>
          <div className="pl-5">

            <div className={filters.label}>RTO</div>
            <Select value={filters.RTO} style={{ width: "100%" }} key="RTO" size="large" onChange={e => this.onSelectFilter("RTO", e)} placeholder="Select">
              <Option value="">Any</Option>
              {this.props.RTO && this.props.RTO.map((e: any) => <Option value={e.id}>{e.city}</Option>)}
            </Select>
          </div>
        </Panel>
        <Panel key="fuel" className={styles.filterSection} header={<div className={styles.sectionHead}>Fuel Type</div>}>
          <div className="pl-5">
            <div className={filters.label}>Fuel</div>
            <Select value={filters.fuel} style={{ width: "100%" }} key="fuel" size="large" onChange={e => this.onSelectFilter("fuel", e)} placeholder="Select">
              {this.props.fuelType && this.props.fuelType.map((e: string) => <Option value={e}>{e}</Option>)}
              <Option value="">Any</Option>
            </Select>
          </div>
        </Panel>

        <Panel key="transmission" className={styles.filterSection} header={<div className={styles.sectionHead}>Transmission</div>}>
          <div className="pl-5">
            <div className={filters.label}>Transmission</div>
            <Select value={filters.transmission} style={{ width: "100%" }} key="transmission" size="large" onChange={e => this.onSelectFilter("transmission", e)} placeholder="Select">
              {this.props.fuelType && this.props.fuelType.map((e: string) => <Option value={e}>{e}</Option>)}
              <Option value="">Any</Option>
            </Select>
          </div>
        </Panel>

        <Panel key="color" className={styles.filterSection} header={<div className={styles.sectionHead}>Color</div>}>
          <div className="pl-5">

            <Radio.Group onChange={e => this.onSelectFilter("color", e.target.value)} value={filters.color}>
              {this.props.color && this.props.color.map(e =>
                <Radio.Button className={styles.colorBotton} value={e} style={{ backgroundColor: e }}></Radio.Button>
              )}
            </Radio.Group>
          </div>
        </Panel>
      </Collapse>
    </>

    return (
      <MDBCol size="12" lg="3" >
        {window.innerWidth >= 992 ?
          <Card hoverable className={styles.FiltersContainer} bodyStyle={{ padding: "0" }}>
            {innerContent}
          </Card>
          :
          <Collapse className={`p-0 ${styles.FiltersContainer}`}>
            <Panel className={styles.MainHeading} header={<span >Filters</span>} key="1" >
              {innerContent}
            </Panel>
          </Collapse>
        }
      </MDBCol>
    )
  }
}

const mapStateToProps = (state: any) => ({
  ...state.filters
})
export default connect<iMapStateToProps, iMapDispatchToProps>(mapStateToProps, { getFilters,getBuyCarsData })(withRouter<any, any>(FiltersContainer));
