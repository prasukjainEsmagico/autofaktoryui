import React from 'react';
import styles from './SignIn.module.scss';
import { Modal, Button } from 'antd';
import { MDBCol, MDBIcon, MDBRow } from 'mdbreact';
import {hideLogin} from 'actions'

import loginImage from 'assets/images/Loginpage.jpg'
import { connect } from 'react-redux';

interface iMapDispatchToProps{
  hideLogin:any
}
interface iProps extends iMapDispatchToProps {

}

interface iState {
  dispalyPhonePage: boolean
  handleIslogged: boolean
  phoneNumber: string
  otp: string
  invalid: string
  msg: string
}

class SignIn extends React.Component<iProps, iState> {
  constructor(props: iProps) {
    super(props);
    this.state = {
      dispalyPhonePage: true,
      handleIslogged: true,
      phoneNumber: '',
      otp: '',
      invalid: '',
      msg: ''
    };
  }

  componentWillUnmount(){
    this.props.hideLogin()
  }

  handleChange = (event: any) => {
    this.setState({ invalid: '', msg: '' });
    let { name, value } = event.target;

    this.setState(
      // @ts-ignore
      { [name]: value }
    );

  };

  editPhoneNo = () => {
    this.setState({ dispalyPhonePage: true });
  };

  handleSendOtp = () => {
    this.setState({ dispalyPhonePage: false });
    // fetch(`${API}/sendotp`,
    //   {
    //     method: 'POST',
    //     headers: {
    //       'Content-Type': 'application/json'
    //     },
    //     body: JSON.stringify({
    //       phoneNumber: `+91${this.state.phoneNumber}`
    //     })
    //   }
    // )
    //   .then(res => {
    //     return res.json();
    //   })
    //   .then(data => {

    //     if (data.success) {
    //       // console.log(">>>>>>>>>>>>>>>>",data)
          this.setState({ dispalyPhonePage: false });
    //     } else {
    //       this.setState({ dispalyPhonePage: true });
    //     }
    //   });
  };

  submitPhoneNo = (e: any) => {

    e.preventDefault();
    if (this.state.phoneNumber) {
      // console.log(e,"i am coming here ")
      /^[\d]{10}$/.test(this.state.phoneNumber)
        ? this.handleSendOtp()
        : this.setState({ invalid: 'Enter a valid Phone Number' })
    }
    else {
      this.setState({ msg: "Phone Number can't be empty" });
    }
  };


  displayPhonePage = () => {
    return (
      <form >
        <label>{this.state.invalid}</label>
        <label>{this.state.msg}</label>
        <label>Enter a phone number</label>
        <input
          type="tel"
          className="form-control"
          name="phoneNumber"
          placeholder="8888888888"
          value={this.state.phoneNumber}
          onChange={this.handleChange}
          required
        />
        <br></br>
        <button type="submit"  onClick={this.submitPhoneNo} className="btn btn-secondary  " >Next</button>
      </form>
    )
  }
  handleVerifyOtp = (e: any) => {
    e.preventDefault();
    // console.log(">>>>>>>>>>>>>>>>>>>>")
    if (this.state.otp) {
    }
    // else {
    this.setState({ msg: "OTP can't be empty" })
    // }
  };


  displayOtpPage = () => {
    return (
      <form >
        <label>{this.state.invalid}</label>
        <label>{this.state.msg}</label>
        <label>Enter OTP</label>
        <input
          type="tel"
          className="form-control mb-2"
          name="otp"
          placeholder="Enter OTP"
          value={this.state.otp}
          onChange={this.handleChange}
          required
        />
        <div className="login-next">
          {/* <button onClick={this.editPhoneNo}>Edit Phone Number </button> */}
          <button onClick={this.handleSendOtp} className="btn "> Resend OTP </button>
          <a href="/used-car">
            <button type="submit" className="btn btn-secondary" onClick={this.handleVerifyOtp} >Next</button>
          </a>

        </div>


      </form>
    )
  }


  render() {
    let { dispalyPhonePage, handleIslogged } = this.state;
    return (
      <Modal
        visible={true}
        title={null}
        footer={null}
        className={styles.SignIn}
        onCancel={this.props.hideLogin}
        width="80vh"
      >
        <MDBRow>
          <MDBCol className="p-3" >
            <h1 >Log In</h1>
            <div className="feature">
              <p >
                <MDBIcon icon="thumbs-up" className="mr-2" />

                Get personalized recommendations
                                </p>
            </div>
            <div className="feature">
              <p >
                <MDBIcon icon="bell" className="mr-2" />
                Receive alerts on newly added cars
                                </p>
            </div>
            <div>
              {/* Signin */}
              <img
                src={loginImage}
                width="80%"
                alt="Auto-factory"
              />
            </div>
          </MDBCol>
          <MDBCol>
            <br></br>
            <br></br>
            <br></br>
            {dispalyPhonePage ? this.displayPhonePage() : this.displayOtpPage()}
            <p style={{ fontSize: "12px" }}>
              By Signing up, I agree to terms and privacy
              policy
</p>
          </MDBCol>
        </MDBRow>
      </Modal>
    )
  }
}

export default connect<null,iMapDispatchToProps>(null,{hideLogin})( SignIn);
