import ReducerType from "common/constants/reducerType";
import { iActionProvider, iFilterReducer,  } from "common/interfaces/interfaces";

const initailState:iFilterReducer = {

    cities:[],
    MakeModelData:{},
    transmission:[],
    fuelType:[],
    year: [],
    RTO:[],
    kms:[],
    color: [],
    maxPrice:1000000
}

export const FilterReducer = (state: iFilterReducer = initailState, action: iActionProvider<iFilterReducer>): iFilterReducer => {

    switch (action.type) {
        case ReducerType.GET_FILTERS:
            return {
                ...state,
                ...action.payload
            }
        default:
            return state
    }
}