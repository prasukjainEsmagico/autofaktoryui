import ReducerType from "common/constants/reducerType";
import { iUserReducer, iActionProvider } from "common/interfaces/interfaces";

const initailState: iUserReducer = {
    user: {
        firstName: "",
        lastName: "",
        email: ""
    },
    showLogin: false
}

export const UserReducer = (state: iUserReducer = initailState, action: iActionProvider<iUserReducer>): iUserReducer => {

    switch (action.type) {
        case ReducerType.SAVE_USER_INFO:
            return {
                ...state,
                ...action.payload
            }
        case ReducerType.SHOW_LOGIN:
            return {
                ...state,
                showLogin:true
            }

        case ReducerType.HIDE_LOGIN:
            return {
                ...state,
                showLogin:false
            }
            
        default:
            return state
    }
}