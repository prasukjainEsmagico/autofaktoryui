import ReducerType from "common/constants/reducerType";
import { iActionProvider, iBuyCarReducer } from "common/interfaces/interfaces";

const initailState: iBuyCarReducer = {
    cars:[],
    count: 0
}

export const BuyCarReducer = (state: iBuyCarReducer = initailState, action: iActionProvider<iBuyCarReducer>): iBuyCarReducer => {

    switch (action.type) {
        case ReducerType.GET_BUY_CARS_DATA:
            return {
                ...state,
                ...action.payload
            }
        default:
            return state
    }
}