import { iBuyCarReducer, iUserReducer } from 'common/interfaces/interfaces';
import {combineReducers} from  'redux';
import { UserReducer } from './user.reducer';
import { BuyCarReducer } from './buyCar.reducer';
import { FilterReducer } from './filters.reducers';
export interface State{
    user:iUserReducer,
    buyCar:iBuyCarReducer,
    filters: any
}

export default combineReducers<State>({
    user:UserReducer,
    filters: FilterReducer,
    buyCar:BuyCarReducer
})
