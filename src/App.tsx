import React from 'react';
import './App.scss';
import Home from 'pages/Home/Home';
import { Redirect, Route, Switch } from "react-router-dom";
import NavBar from 'components/NavBar/NavBar';
import SignIn from 'components/SignIn/SignIn';
import Footer from 'components/common/Footer/Footer';
import Blogs from 'pages/Blogs/Blogs';
import FAQs from 'pages/FAQs/FAQs';
import ComingSoon from 'pages/ComingSoon/ComingSoon';
import { connect } from 'react-redux';
import HowItWorks from 'pages/HowItWorks/HowItWorks';
import RecommendedCars from 'components/common/RecommendedCars/RecommendedCars';
import SellCar from 'pages/SellCar/SellCar';
import BuyCar from 'pages/BuyCar/BuyCar';
import BuyCarDetails from 'pages/BuyCarDetails/BuyCarDetails';

interface iMapStateToProps {
  showLogin: boolean
}
interface iProps extends iMapStateToProps {
  showLogin: any
}
interface iState {

}

class App extends React.Component<iProps, iState> {
  render() {
    const token = localStorage.getItem("token")
    return (
      <div className="App">
        <NavBar />
        <div className="main">
          <Switch>
            <Route path="/blogs" component={Blogs} />
            <Route path="/faq" component={FAQs} />
            <Route path="/howitworks" component={HowItWorks} />
            <Route path="/buy"  component={BuyCar} >
              
            </Route>
            <Route path="/sell" component={SellCar} >
            </Route>
            <Route path="/details/:id" component={BuyCarDetails} >
            </Route>
            <Route path="/comingsoon" component={ComingSoon} />
            <Route exact path="/" component={Home} />
            <Route path="/" component={Home} />
          </Switch>
          <RecommendedCars />
          <Footer />

        </div>
        {this.props.showLogin && !token &&
          <SignIn />
        }
      </div>
    );
  }
}
function mapStateToProps(state: any) {
  return {
    showLogin: state.user.showLogin
  }
}

export default connect<iMapStateToProps, null>(mapStateToProps, null)(App);
