
export interface iUser {
    firstName: string
    lastName: string
    email: string
}

export interface iActionProvider<T> {
    type: string
    payload: Partial<T>
}


export interface iUserReducer {
    user: iUser
    showLogin: boolean
}


export interface iBuyCarReducer {
    cars: any[]
    count: number
}


export interface iFilterReducer {
    cities: string[]
    MakeModelData: any
    transmission: string[]
    fuelType: string[]
    year: number[]
    RTO: any[]
    kms: number[]
    color: string[]
    maxPrice: number
}


export interface iBuyCarFeatures {
    id: number,
    feature__name: string,
    value: string
}
export interface iCarDetailsOtherSpecs {

    id: number
    car: number
    drivetrain: number
    engineType: number
    valveCylenderConfig: number
    maxPower: number
    maxTorque: number
    suspensionFront: number
    suspensionRear: number
    steeringType: number
    wheels: number
    spareWheels: number
    frontBreakType: number
    rearBreakType: number
    frontTyre: number
    rearTyre: number
    dimLength: number
    dimWidth: number
    dimHeight: number
    seetingCapacity: number
    displacement: number
    wheelbase: number
    groundClearance: number
    kerbWeight: number
    door: number
    seetingRows: number
    bootSpace: number
    fuelCapacity: number
    mileage: number
    turningRadius: number

}
export interface iCarDetails {

    id: 1
    make: string
    model__model: string
    varient__name: string
    varient__fuelType: string
    registrationYear: number
    locality: string
    price: number
    condition: string
    km: number
    transmission: string
    RTO__city: string
    owner: string
    color: string
    isInsuranceValid: string
    insuranceValidity: string
    other_specs: iCarDetailsOtherSpecs
    image_url: string
    features: iBuyCarFeatures[]

}