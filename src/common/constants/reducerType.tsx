const   GET_AUTH_DATA = "GET_AUTH_DATA"
const   SAVE_USER_INFO = "SAVE_USER_INFO"
const   SHOW_LOGIN = "SHOW_LOGIN"
const   HIDE_LOGIN = "HIDE_LOGIN"
const   GET_BUY_CARS_DATA = "GET_BUY_CARS_DATA"
const   GET_FILTERS = "GET_FILTERS"


const reducerType=  {
    GET_AUTH_DATA,
    SAVE_USER_INFO,
    SHOW_LOGIN,
    HIDE_LOGIN,
    GET_BUY_CARS_DATA,
    GET_FILTERS
}
export default reducerType