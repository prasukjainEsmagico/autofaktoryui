import axios from "axios"
import reducerType from "common/constants/reducerType"

export const getCities = ()=>  (dispatch:any):Promise<string> => {
    return new Promise((resolve: any, reject: any) => {
        axios.get(`/common/city`).then((res: any) => {
            if(res.data.cities)
            dispatch({
                type: reducerType.GET_FILTERS,
                payload:res.data
            })
            return resolve(res.data.cities)
            throw new Error("Could not fetch city")
        }).catch((e:Error)=>console.error(e))
    })
}


export const getFilters = ()=>  (dispatch:any):Promise<string> => {
    return new Promise((resolve: any, reject: any) => {
        axios.get(`/buy/filters`).then((res: any) => {
            if(res.data)
            console.log(res.data);
            dispatch({
                type: reducerType.GET_FILTERS,
                payload:{...res.data}
            })
            return resolve(res.data)
            throw new Error("Could not fetch city")
        }).catch((e:Error)=>console.error(e))
    })
}