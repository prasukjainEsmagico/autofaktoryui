import axios from "axios"
import reducerType from "common/constants/reducerType"
import { iCarDetails } from "common/interfaces/interfaces"


export const getBuyCarsData = (query: string = "") => (dispatch: any): Promise<string> => {
    return new Promise((resolve: any, reject: any) => {
        const city = localStorage.getItem('city')
        query += "&city=" + city
        axios.get(`/buy/list?${query}`).then((res: any) => {
            if (res.data)
                console.log(res.data);
            dispatch({
                type: reducerType.GET_BUY_CARS_DATA,
                payload: {
                    count: res.data.count,
                    cars: res.data.values
                }
            })
            return resolve(res.data)
            throw new Error("Could not fetch city")
        }).catch((e: Error) => console.error(e))
    })
}

export const getCar = (id: string) => (dispatch: any): Promise<iCarDetails> => {
    return new Promise((resolve: any, reject: any) => {
        axios.get(`/buy/get/${id}`).then((res: any) => {
            if (res.data)
            return resolve(res.data)
            throw new Error("Could not fetch Car")
        }).catch((e: Error) => alert(e))
    })
}

interface iTestDriveData{
    date: Date
    time_slot: string
    otp: string
    car: number|string
}
export const sendTestDriveData = (phone:string,data:iTestDriveData)=>()=>new Promise((resolve:any,reject:any)=>{
    axios.post(`/buy/otp/91${phone}`,data)
    .then((res)=>resolve())
    .catch(e=>reject(e))
})