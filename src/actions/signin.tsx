import axios from "axios"
import reducerType from "common/constants/reducerType"

export const showLogin= ()=>({
    type : reducerType.SHOW_LOGIN,
})


export const hideLogin= ()=>({
    type : reducerType.HIDE_LOGIN,
})

export const getOtp = (phone:string)=>(dispatch:any)=>new Promise((resolve:any,reject:any)=>{
    axios.get(`/buy/otp/91${phone}`).then((res)=>resolve())
    .catch(e=>reject(e))
})